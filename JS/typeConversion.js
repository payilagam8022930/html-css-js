//Type conversion starts

//1.number to String 

/*let a
a=25
console.log(a,typeof a)
a=String(a)
console.log(a,typeof a)

a=25.55
console.log(a,typeof a)
a=String(a)
console.log(a,typeof a)

a=true
console.log(a,typeof a)
a=String(a)
console.log(a,typeof a)

a=new Date()
console.log(a,typeof a)
a=String(a)
console.log(a,typeof a)

a=[1,2,3,4,5]
console.log(a,typeof a)
a=String(a)
console.log(a,typeof a)

a=25
console.log(a,typeof a)
a=a.toString()
console.log(a,typeof a)*/

//String to number

/*let a="125"
console.log(a,typeof a)
a=Number(a)
console.log(a,typeof a)

a=true
console.log(a,typeof a)
a=Number(a)
console.log(a,typeof a)

a=false
console.log(a,typeof a)
a=Number(a)
console.log(a,typeof a)

a=[1,2,3,4,5]
console.log(a,typeof a)
a=Number(a)
console.log(a,typeof a) //NaN--->not a number

a="yogi"
console.log(a,typeof a)
a=Number(a)
console.log(a,typeof a) //NaN--->not a number 

a="35"
console.log(a,typeof a)
a=parseInt(a)
console.log(a,typeof a)

a="35.555"
console.log(a,typeof a)
a=parseInt(a)
console.log(a,typeof a)

a="35.55"
console.log(a,typeof a)
a=parseFloat(a)
console.log(a,typeof a)*/

//Type conversion ends
