const button = document.querySelector('button')
const resultdiv = document.createElement('div')
resultdiv.id='result'
document.getElementById('wrapper').appendChild(resultdiv)

button.addEventListener('click',display)
function display(){
    const input = document.getElementById('input')
    const city = input.options[input.selectedIndex].value
    
    let population = 0,literacyRate=0;language=''
    switch(city){
        case 'Chennai':
            population = 11933000
            literacyRate = 2.37
            language = 'Tamil'
            break;
        case 'Bengaluru':
            population = 13608000
            literacyRate = 3.15
            language = 'Kannada'
            break;
        case 'Mumbai':
            population = 21296517
            literacyRate = 1.6
            language = 'Marathi'
            break;

    }
    let text =`The current population of ${city} is ${population} as of today, based on worldometer elaboration of the latest united Nations data. literacyRate is ${literacyRate} People who spoken language is ${language}.`
    document.getElementById('result').innerHTML=text

}
