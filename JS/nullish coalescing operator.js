//Nullish coalescing operator starts  (??)
/*
const a=null??'No Value'
console.log(a)
const b=22??45
console.log(b)

//??=
const user ={Name:'yogi'}
console.log(user)
console.log(user.Name)
user.city??='chennai'
console.log(user.city)
console.log(user)

const man ={Name:"Manohar"}
console.log(man)
man.area??="Madipakkam"
console.log(man.area)
console.log(man)*/



//Nullish coalescing operator ends  (??)
