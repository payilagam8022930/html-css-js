//1.console syntax printing starts.

//alert("warning")
// console.log("welcome to learn js")
// console.log(123456)
// console.log(1.1,2.2,3.3,4.4,5.5)
// console.log(true)
// console.log([58,35,45,55,98,88])
// console.log(["yogeshwaran","santhosh","sowneshvel"])
// console.log({Name:"yogeshwaran",age:21})
// console.table({Name:'yogeshwaran',age:21})
// console.error("custom sample error")
// console.warn("custom sample warning")
// console.clear()
// console.time("Timer")
// for(i=0;i<=10;i++)
// {
//     console.log(i)
// }
// console.timeEnd("Timer")


//console syntax printing ends.

//2. let //var //const starts.

// var a=25
// var b=35
// console.log(a+b)

// if(true){
//     var msg="var welcome to js learnig"
// }
// console.log(msg)//acting as global variable.
// if(true){
//     let ing="let welcome to js learnig"
//     console.log(ing)//acting as local variable in a scope.
// }
// if(true){
//     const ing="const welcome to js learnig"
//     console.log(ing) //so const also works like let as local variable in a scope only
// }


// var a=25
// console.log(a)

// var a =45;
// console.log(a)

// let a=25
// console.log(a)
// let a=45
// console.log(a)  //so it leads to syntax error so it doesnt allows to redeclare the values again.

// const a =25
// console.log(a)
// const a=45 //it also same as like let it doesnt allows to redeclare the values again.

// let //var //const ends.


//3.vlaue assignment starts

/*var a =25
console.log(a)
a=55
console.log(a)*/ // it doesnt leads to any error.

/*let a=25
console.log(a)
a=65
console.log(a)*/ //this also works like var in this format.

/*const a=25
console.log(a)
a=65
console.log(a)*/  //so here we cant reassign a value in const.

/*const student={Name:"yogi",age:21}
console.table(student)
console.table(student.Name)
student.Name="D.yogeshwaran"
console.table(student)*/  //so here we can reassign a value or key in an object at const value also.

/*let student={Name:"yogi",age:21}
console.table(student)
console.table(student.Name)
student.Name="D.yogeshwaran"
console.table(student)*/ // in let also we can use like const. 

/*var student={Name:"yogi",age:21}
console.table(student)
console.table(student.Name)
student.Name="D.yogeshwaran"
console.table(student)*/  //but here if we ctry to do these the syntax error will occurs

// value asssignment ends

//4.Data types in javascript starts

// js is dynamic programming language

/* 
-->inbuilt data types
1.Number type eg:1.25,25,3.22,32.555
2.Boolean type eg: True False
3.Null
4.Undefined
5.Symbols intrduced in E6

-->reference data types
1.Arrays
2.Object
3.Date
4.Time
 */


//inbuilt data type starts
//Number type

/*var a=25.5
console.log(typeof a)
var Name="yogi"
console.log(typeof Name)
var isMarried=true
console.log(typeof isMarried)
var phone=null
console.log(typeof phone)

const s1=Symbol
console.log(s1)

const s2=Symbol
console.log(s2)

console.log(s1==s1)*/ //why true it should be false ask to santhosh

//inbuilt data type ends

//reference data type starts

/*var course=['java','python','javascript']
console.log(typeof course)
var student={Name:'yogi',age:21}
console.log(typeof student)
var d = new Date()
console.log(typeof d)
console.log(d)*/

//referance data type ends
//Data types in javascript ends


//Type conversion starts

//1.number to String 

/*let a
a=25
console.log(a,typeof a)
a=String(a)
console.log(a,typeof a)

a=25.55
console.log(a,typeof a)
a=String(a)
console.log(a,typeof a)

a=true
console.log(a,typeof a)
a=String(a)
console.log(a,typeof a)

a=new Date()
console.log(a,typeof a)
a=String(a)
console.log(a,typeof a)

a=[1,2,3,4,5]
console.log(a,typeof a)
a=String(a)
console.log(a,typeof a)

a=25
console.log(a,typeof a)
a=a.toString()
console.log(a,typeof a)*/

//String to number

/*let a="125"
console.log(a,typeof a)
a=Number(a)
console.log(a,typeof a)

a=true
console.log(a,typeof a)
a=Number(a)
console.log(a,typeof a)

a=false
console.log(a,typeof a)
a=Number(a)
console.log(a,typeof a)

a=[1,2,3,4,5]
console.log(a,typeof a)
a=Number(a)
console.log(a,typeof a) //NaN--->not a number

a="yogi"
console.log(a,typeof a)
a=Number(a)
console.log(a,typeof a) //NaN--->not a number 

a="35"
console.log(a,typeof a)
a=parseInt(a)
console.log(a,typeof a)

a="35.555"
console.log(a,typeof a)
a=parseInt(a)
console.log(a,typeof a)

a="35.55"
console.log(a,typeof a)
a=parseFloat(a)
console.log(a,typeof a)*/

//Type conversion ends


//Type coercion starts.

/*let a=10
let b="55"
console.log(a+b)  //if we are adding a string and integer or number then it will conctas here.

b=Number(b)
console.log(a+b)*/   //so here we changed the type of b value from string to number so here it additions it

//Type coercion ends


//Arithmetic operators starts

/*let a=25
let b=55
let c;
c=a+b
c=a-b
c=a*b
c=a/b
c=a%b

c=2**2
c=++a
c=--b


console.log(c)*/

//Arithmetic operators ends.

//Assignment operators starts.
/*let a=10
a+=5 //15
a-=5 //10
a*=5 //50
a/=5 //10
a%=5 //0
console.log(a)*/
//Assignment operators ends.

//Math Fucntions starts.
/*let c;
c=Math.PI
c=Math.E
c=Math.round(5.8)//--->6
c=Math.round(5.3)//--->5
c=Math.floor(5.3)//--->5
c=Math.ceil(5.3)//--->6
c=Math.sqrt(49)//--->7 (to find square root).
c=Math.abs(-90)//--->90--changes the negative value to positve.
c=Math.trunc(2.38)//--->it removes the decimal.
c=Math.pow(8,3)//--->it powers the given value.
c=Math.min(10,20,30,40,50,60)//--->to find min value.
c=Math.max(10,20,30,40,50,60)//--->to find max value
c=Math.random()//--->to generate any random decimal values.
c=Math.floor((Math.random()*50+1))//--->to generate any values between 1to50.
c=Math.sign(0)//--->0(to find the sign of given value.)
c=Math.sign(25)//--->1
c=Math.sign(-400)//--->-1
c=Math.sin(45)
c=Math.cos(45)
c=Math.tan(45)
c=Math.log(45)
c=Math.log2(45)
c=Math.log10(50)

console.log(c)*/

//Math function ends.

//comparison operator starts

/*let a=10
let b=10
console.log(a==b)
let c=10
let d="10"
console.log(c===d)
let e=10
let f="25"
console.log(e!=f)*/

//Comparison operator ends.

//Relational operator starts.

/*
>   greater than
<   lesser than
>=  greater than or equal to
<=  lesser than or equal to
*/


/*
let a=20
let b=10
console.log(a>b)

let c=20
let d=10
console.log(c<d)

let e=20
let f=20
console.log(e<=f)

let g=20
let h=20
console.log(g>=h)*/

//Relational operators ends.

//logical operator starts
/*let mark=30
console.log(mark>=35 && mark<=100)
console.log(mark>=35 || mark<=100)
let a=5
console.log(a==2 || a==5)

a=true
console.log(!a)*/

//logical operators ends.

//Strict equality or identity operators starts.

/*let a=10
console.log(a)
let b="10"
console.log(a==b)
console.log(a===b)*/


//strict equality or identity operators ends.

//Ternary operator starts

/*const age =25;

const result=age>=18?"Eligible":"Not Eligible"
console.log(result)


function welcome(Name){
    const result =Name?Name:"No Name"
    console.log("welcome "+result)
}
welcome()
welcome(null)
welcome("yogi")
//Handling null values starts
user ={'name':'yogi','age':25}
console.table(user)
console.log(user.name)


const greeting=()=>{
    const name =user?user.name:"No name"
    return "Hello "+name
}
console.log(greeting())
//Handling null value ends.


//condtional chains


// avg>=90 A grade
// avg>=80 B grade
// C grade 

const avg=92
const grade=avg>=90?"A grade":avg>=80?"B grade":"C grade"
console.log("Grade :"+grade)*/
//conditional chain ends.
//Ternay operator ends.

//Bitwise operator starts.
/* 
& --->Bitwise AND
| --->Bitwise OR
~ --->Bitwise NOT
^ --->Bitwise XOR
<< --->Left Shift
>> --->Right Shift
>>> --->Unsigned Shift 
*/
/*
//Bitwise AND &
let a=12
let b=24
console.log(a&b)//--->output 2
a&=b
console.log(a)

//Bitwise OR |
a=12
b=24
console.log(a|b)
a|=b
console.log(a)

//Bitwise NOT ~
a=12
console.log(~a)

//Bitwise XOR ^
a=12
b=6
console.log(a^b)
a^=a
console.log(a)

//Left shift <<
a=5
b=2
console.log(a<<b)
//Right shift >>
console.log(a>>b)
//Unsigned right shift  >>>
console.log(a>>>b)*/

//Bitwise operator ends.


//Nullish coalescing operator starts  (??)
/*
const a=null??'No Value'
console.log(a)
const b=22??45
console.log(b)

//??=
const user ={Name:'yogi'}
console.log(user)
console.log(user.Name)
user.city??='chennai'
console.log(user.city)
console.log(user)

const man ={Name:"Manohar"}
console.log(man)
man.area??="Madipakkam"
console.log(man.area)
console.log(man)*/



//Nullish coalescing operator ends  (??)


/*for(var i=5;i>=0;i--){
    for(var j=5;j>=i;j--){
        document.write("*"+" ")
    }
    document.write("<br>")

}*/



//Increment(++) or Decrement(--) operator starts.
/*let a=1
a++
console.log(a)

let b=5
b--
console.log(b)

//postfix increment and prefix increment
//postfix increment
let x=3
const y=x++
console.log("Y: ",y,"X: ",x)
//prefix increment
let n=5
const h=++n
console.log("N: ",n,"H: ",h)
//postfix decrement
let i=15
let j=i--
console.log("I: ",i,"J: ",j)
//prefix decrement
let d=5
const e=--d
console.log("E: ",e,"D ",d)*/
//Increment(++) or Decrement(--) operator ends.

//If Else statement starts.
/*
let age=prompt("Enter your age: ")
if(age!=null&&age>=18){
    console.log("you are eligible for vote : "+age)
}
else {
    console.log("You are not eligible for vote : "+age)
}*/

//If Else statement ends.

//If else if ladder starts 

//Example 1
/*let number = prompt("Enter your number: ")
if(number<0){
    console.log(number+" : is negative number")
}
else if(number>0){
    console.log(number," : is positive number")
}
else{
    console.log(number," :Given number is zero")
}*/


//Example 2
/*let avg=prompt("Enter the marks : ")
if(avg>=90&&avg<=100){
    console.log("Very Very good da parama"+" Grade A")
}
else if(avg>=80&&avg<=89){
    console.log("Very good da parama"+" Grade B")
}
else if(avg>=70&&avg<=79){
    console.log("good da parama"+" Grade C")
}
else{
    avg<70
    console.log("poi padi da parama"+" Fail")
}
*/
//If else if ladder ends 

//Nested if Statement starts
/*let english=95,tamil=95,maths=95
let total,avg
total=english+tamil+maths
avg=total/3
// avg=Math.round(avg)
console.log("Total :"+total)
console.log("Average :"+avg.toFixed(2))

if(english>=35 && tamil>=35 && maths>=35){
    console.log("Result : Pass")
    if(avg>90 && avg<=100){
        console.log("Grade : A Grade")
    }
    else if(avg>80 && avg<=90){
        console.log("Grade : B Grade")
    }
    else if(avg>70 && avg<=80){
        console.log("Grade : C Grade")
    }
    else{
        console.log("Grade : D Grade")
    }
}
else{
    console.log("Result : Fail")
    console.log("Grade : No Grade")
}*/
//Nested If statement ends.

/*let n=10
if(n&1==1){
    console.log("Odd number")
}
else{
    console.log("Even number")
}*/

//switch case starts.
/*let num=2
switch(num){
    case 1:
        console.log("ONE")
    break

    case 2:
        console.log("TWO")
    break

    case 3:
        console.log("THREE")
    break

    default:
        console.log("NOT A VALID INPUT")
    break
}*/
//switch case ends.

//Combining case statement starts
/*let letter=prompt("Enter an alphabet : ")
switch(letter){

case 'a':
case 'e':
case 'i':
case 'o':
case 'u':


case 'A':
case 'E':
case 'I':
case 'O':
case 'U':

    console.log(letter+" is an vowel")
    break
default:
    console.log(letter+" is not an vowel")
    break
}*/
//combining case statement ends.

//Looping Statement starts. 

//While loop starts.
/*let i =0
while(i<=5){
    console.log(i)
    i++;
}
//While loop ends.

//do while starts
let table=21;limit=5;i=1;
do{
    console.log(table+" X "+i+" = "+(table*i))
    i++
}while(i<=limit)
console.log("-----------")

let tables=252;limits=15;j=1;
do{
    console.log(tables+" X "+j+" = "+(tables*j))
    j++
}while(j<=limits)
//do while loop ends.

//For loop starts 
for(let i=1;i<=10;i++){
    console.log(i)
}
let arr = []
for(let i=0;i<=100;i++){
    arr.push(i)
    console.log(arr)
}

//For loop ends.

//Nested for loop starts.
for(let i=1;i<=7;i++){
    for(let j=1;j<=i;j++){
        if(i==1 || i==3 || i==5 || i==7){
            document.write(1+" ")
        }
        else{
            document.write("* ")
        }
    }
    document.write("<br>")
}

let nums =[]
for(let i=0;i<3;i++){
    nums.push([])
for(let j=0;j<3;j++){
nums[i].push[j]

}

}
console.log(nums)
console.table(nums)

//Nested for loop ends.


//for of loop starts.
let names =["yogi","sandy","Sownesh","vignesh"]
for(Name of names ){
    console.log(Name)
}
//Nested for loop ends.

//For in loop starts.
let user={
Name:"yogi",
age:21,
city:"Chennai",
contact:"6383774571"
}
for(let prop in user){
    console.log(prop+" : "+user[prop])
}
//For in loop ends.

let user ={
    Name:"Yogeshwaran",
    Age:21,
    DOB:"22/march/2002",
    Contact:"6383774571"
}
let arr_keys=Object.keys(user)
console.table(arr_keys)
let arr_values=Object.values(user)
console.table(arr_values)

for(i=0;i<arr_keys;i++){
    console.log(arr_keys[i]+" : "+arr_values[i])
}*/
//Looping statement ends.

//Break statement starts
/*for(i=1;i<=10;i++){
    console.log(i)
    if(i==4){
        break
    }
}
//Break statement ends.
console.log("--------------------------")
//Continue statement starts.
for(i=1;i<=10;i++){
   
    if(i%2==0){
        continue
    }
    console.log(i)
}*/

//Continue statement ends.